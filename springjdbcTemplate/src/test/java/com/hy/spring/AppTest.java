package com.hy.spring;

import static org.junit.Assert.assertTrue;

import com.hy.dao.EmpDao;
import com.hy.entity.Emp;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {

        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
//        JdbcTemplate jdbcTemplate = context.getBean("jdbcTemplate", JdbcTemplate.class);
//        System.out.println(jdbcTemplate);

        EmpDao empDao = (EmpDao) context.getBean("empDao");
//        Emp emp = empDao.selectEmpById(1);
//        System.out.println(emp);

//        List<Emp> emps = empDao.selectEmps();
//        System.out.println(emps);

//        List list = empDao.selectEmpsByList();
//        System.out.println(list);
//        int result = empDao.updateById("新小梅");

        Emp emp = new Emp();
        emp.setId(1);
        emp.setName("小梅");
        emp.setSalary("10000");

//        int result = empDao.deleteById(1);
        int result = empDao.addEmp(emp);
        System.out.println("添加的行数==="+result);
    }
}
